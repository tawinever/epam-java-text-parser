package com.example.parser;

import com.example.parser.fetcher.*;
import com.example.letterComposite.Text;

public class TextParser {
    private Fetcher chain;

    public TextParser() {
        buildChain();
    }

    private void buildChain() {
        chain = new ParagraphFetcher(new SentenceFetcher(new WordFetcher(new LetterFetcher(null))));
    }

    public void process(String text, Text container) {
        for (int i = 0; i < text.length(); ++i) {
            chain.handleCharacter(text.charAt(i), container);
        }
    }
}

package com.example.parser.enums;

public enum FetcherState {
    ACTIVE,
    NON_ACTIVE;
}

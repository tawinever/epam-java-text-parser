package com.example.parser.fetcher;

import com.example.parser.enums.FetcherState;
import com.example.letterComposite.Paragraph;
import com.example.letterComposite.Text;

import java.util.Collections;
import java.util.List;

public class ParagraphFetcher extends Fetcher {

    public ParagraphFetcher(Fetcher next) {
        super(next);
    }

    @Override
    protected List<Character> getFinishers() {
        return Collections.singletonList('\n');
    }

    @Override
    public void handleCharacter(char character, Text root) {
        if (getFinishers().contains(character)) {
            if(this.state == FetcherState.ACTIVE) {
                this.state = FetcherState.NON_ACTIVE;
                LOGGER.debug("Paragraph end.");
            }
        } else if (this.state == FetcherState.NON_ACTIVE) {
            root.add(new Paragraph());
            this.state = FetcherState.ACTIVE;
            LOGGER.debug("Paragraph start.");
        }
        super.handleCharacter(character, root);
    }

}

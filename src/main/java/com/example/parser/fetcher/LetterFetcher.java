package com.example.parser.fetcher;

import com.example.letterComposite.Letter;
import com.example.letterComposite.Text;

import java.util.Arrays;
import java.util.List;

public class LetterFetcher extends Fetcher {
    public LetterFetcher(Fetcher next) {
        super(next);
    }

    @Override
    protected List<Character> getFinishers() {
        return Arrays.asList('\n', '.', ' ', '\t');
    }

    @Override
    public void handleCharacter(char character, Text root) {
        if (!getFinishers().contains(character)) {
            root.getCurrent().getCurrent().getCurrent().add(new Letter(character));
        }
        super.handleCharacter(character, root);
    }
}

package com.example.parser.fetcher;

import com.example.parser.enums.FetcherState;
import com.example.letterComposite.Sentence;
import com.example.letterComposite.Text;

import java.util.Arrays;
import java.util.List;

public class SentenceFetcher extends Fetcher {

    public SentenceFetcher(Fetcher next) {
        super(next);
    }

    @Override
    protected List<Character> getFinishers() {
        return Arrays.asList('\n', '.');
    }

    @Override
    public void handleCharacter(char character, Text root) {
        if (getFinishers().contains(character)) {
            if (this.state == FetcherState.ACTIVE) {
                this.state = FetcherState.NON_ACTIVE;
                LOGGER.debug("Sentence end.");
            }
        } else if (this.state == FetcherState.NON_ACTIVE) {
            root.getCurrent().add(new Sentence());
            this.state = FetcherState.ACTIVE;
            LOGGER.debug("Sentence start.");
        }
        super.handleCharacter(character, root);
    }
}

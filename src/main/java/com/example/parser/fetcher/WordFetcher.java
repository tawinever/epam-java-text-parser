package com.example.parser.fetcher;

import com.example.parser.enums.FetcherState;
import com.example.letterComposite.Text;
import com.example.letterComposite.Word;

import java.util.Arrays;
import java.util.List;

public class WordFetcher extends Fetcher {
    public WordFetcher(Fetcher next) {
        super(next);
    }

    @Override
    protected List<Character> getFinishers() {
        return Arrays.asList('\n', '.', ' ');
    }

    @Override
    public void handleCharacter(char character, Text root) {
        if (getFinishers().contains(character)) {
            if (this.state == FetcherState.ACTIVE) {
                this.state = FetcherState.NON_ACTIVE;
                LOGGER.debug("Word end");
            }
        } else if (this.state == FetcherState.NON_ACTIVE) {
            root.getCurrent().getCurrent().add(new Word());
            this.state = FetcherState.ACTIVE;
            LOGGER.debug("Word start");
        }
        super.handleCharacter(character, root);
    }
}

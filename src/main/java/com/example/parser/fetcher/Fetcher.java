package com.example.parser.fetcher;

import com.example.parser.enums.FetcherState;
import com.example.letterComposite.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public abstract class Fetcher {
    protected static final Logger LOGGER = LogManager.getLogger(Fetcher.class);
    private Fetcher next;
    protected FetcherState state;

    public Fetcher(Fetcher next) {
        this.next = next;
        this.state = FetcherState.NON_ACTIVE;
    }

    protected abstract List<Character> getFinishers();

    public void handleCharacter(char character, Text root) {
        if (next != null) {
            next.handleCharacter(character, root);
        }
    }
}



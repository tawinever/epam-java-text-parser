package com.example;

import com.example.letterComposite.LetterComposite;
import com.example.letterComposite.Text;
import com.example.parser.TextParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Demo {
    private static final Logger LOGGER = LogManager.getLogger(Demo.class);

    public static void main(String[] args) {
        String text = "\tIt has survived not only five centuries, but also the leap into electronic typesetting, " +
                "remaining essentially unchanged. It was popularised in the with the release of Letraset sheets " +
                "containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus " +
                "PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable content of a " +
                "page when looking at its layout. The point of using Ipsum is that it has a more-or-less normal " +
                "distribution of letters, as opposed to using 'Content here, content here', making it look like " +
                "readable English.\n" +
                "\tIt is an established fact that a reader will be of a page when looking at its layout.\n" +
                "\tBye.";

        TextParser parser = new TextParser();
        Text container = new Text();

        try {
            parser.process(text, container);
        } catch (IndexOutOfBoundsException ex) {
            LOGGER.info("Occurred mistake while parsing text. Check the format of text.");
        }

        List<LetterComposite> paragraphs = container.getSortedChildren();

        LOGGER.info("Here is sorted paragraphs:\n");

        int cnt = 0;
        for (LetterComposite paragraph: paragraphs) {
            LOGGER.info("{}. Paragraph is:\n {}", ++cnt, paragraph.toString());
        }

        List<LetterComposite> sentences = paragraphs.get(paragraphs.size() - 1).getSortedChildren();
        cnt = 0;
        for (LetterComposite sentence: sentences) {
            LOGGER.info("{}. Sentence is:\n {}", ++cnt, sentence.toString());
        }

        List<LetterComposite> words = sentences.get(sentences.size() - 1).getSortedChildren();
        cnt = 0;
        for (LetterComposite word: words) {
            LOGGER.info("{}. Word is:\n {}", ++cnt, word.toString());
        }
    }
}

package com.example.letterComposite;

public class Letter extends LetterComposite {
    private char character;

    public Letter(char character) {
        this.character = character;
    }

    @Override
    public String beforeToString() {
        return String.valueOf(this.character);
    }
}

package com.example.letterComposite;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class LetterComposite {
    private List<LetterComposite> children = new ArrayList<>();

    public void add(LetterComposite letter) {
        children.add(letter);
    }

    public List<LetterComposite> getSortedChildren() {
        List<LetterComposite> sortedChildren = new ArrayList<>(children);

        sortedChildren.sort(Comparator.comparingInt((LetterComposite o) -> o.children.size()));
        return sortedChildren;
    }

    public LetterComposite getCurrent() throws IndexOutOfBoundsException{
        if (children.isEmpty())
            throw new IndexOutOfBoundsException();
        return children.get(children.size() - 1);
    }

    public String beforeToString() {
        return "";
    }

    public String afterToString() {
        return "";
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.beforeToString());
        for (LetterComposite child : children) {
            stringBuilder.append(child.toString());
        }
        stringBuilder.append(this.afterToString());
        return stringBuilder.toString();
    }

}
